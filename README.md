
## Forlorn Wail

Cloned **EarthQuaker Devices<sup>®</sup>** [Ghost Echo](https://www.earthquakerdevices.com/ghost-echo) reverb pedal based off **PedalPCB<sup>®</sup>** [Spirit Box](https://www.pedalpcb.com/product/spiritbox/) design.

![pcb](doc/ForlornWail-pcb.png)
